package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String answer = "";
        int number = 0;


        do {
            System.out.println("Put the number");
             number = scanner.nextInt();
            if (number % 2 == 0) {
                System.out.println(number + " is even");
            } else {
                System.out.println(number + " is odd");
            }
            System.out.println("Do you want to continue?");
            answer = scanner.next();

        } while (!answer.equalsIgnoreCase("koniec"));

    }
}
